
# Kubernetes Deployment with Ansible

> Sofiane Benahmed

## Overview

This is an ansible playbook that deploys docker and a single-node kubernetes cluster on a remote host and deploys and smaple application on it.

For the roles we use the ones from user `geerlingguy` from github:

- **`docker` role**: https://github.com/geerlingguy/ansible-role-docker.git 

- **`kubernetes` role**: https://github.com/geerlingguy/ansible-role-kubernetes.git 

The custom role (**`deploy` role**) does the following:

- Installs the python dependencies for `k8s` and `docker_image` ansible modules

- Pulls the code from github and build a docker image

- Deploys the pods on kubernetes as a replica set of 2

- deploys a service with the nodeport configuration (uses round-robin load balancing by default)

The app is accessible via http://YOUR_SERVER_IP:30080


## Setup

To run the playbook, get the code:
```bash
git clone https://sofianBnh@bitbucket.org/sofianBnh/k8s-ansible-playbook.git --recursive
cd k8s-ansible-playbook
```

Add the private key as `keys/id_ansible_rsa` or remove this line from `ansible.cfg` if you want to use your host private key:
```ini
private_key_file = keys/id_rsa_ansible
```

Change the host ip for the node in `hosts.ini`:
```ini
[nodes]

192.168.122.101
```

Run the playbook:
```bash
ansible-playbook main.yml
```

Variables can be changed in `vars.yml`

## Sources

- https://github.com/geerlingguy
- https://github.com/geerlingguy/ansible-for-devops